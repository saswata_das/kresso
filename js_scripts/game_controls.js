
// import and load koji configs
/*import Koji from 'koji-tools';
Koji.pageLoad();
*/

var hero_piece;
var life_left = 3;
var obstacle_list = [], eatable_list=[];
var frame_speed = 9, game_score = 0, game_level = 1;
var max_obstacle_per_frame = 4, min_obstacle_per_frame = 3, last_iteration = 0;
var canvas_left = 1, canvas_right = 500, canvas_top = 1, canvas_bottom = 490;
var game_play_flag = false;
var soundFlag = true;

function startGame() {
    if(!game_play_flag && life_left > 0){
        var gameButton = document.getElementById('game_button');
        gameButton.setAttribute('value', 'End Game');

        game_play_flag = true;
        fillCanvas(); 
    }else if(game_play_flag || life_left < 0){
        game_space.stop();
        obstacle_list = [];  
        eatable_list = [];
        game_space.clear();
        game_play_flag = false;
        game_score = 0;
        game_level = 1;
        life_left = 3;

        var score_board = document.getElementById('score_board');
        score_board.innerHTML = "Score: "+game_score;

        var ctx = spaceCanvas.getContext("2d");
        ctx.font = "bold 50px Comic Sans MS";
        ctx.fillStyle = "magenta";
        ctx.fillText("Game Ready", 100, 250);

        var gameButton = document.getElementById('game_button');
        gameButton.setAttribute('value', 'New Game');
    }
}

function reportCrash(){
    life_left -= 1

    if(life_left < 1){
        var gameButton = document.getElementById('game_button');
        gameButton.setAttribute('value', 'Resfresh Game');
    }else{
        game_space.stop();
        obstacle_list = [];  
        eatable_list = [];
        game_space.clear();
        game_play_flag = false;

        var gameButton = document.getElementById('game_button');
        gameButton.setAttribute('value', 'Continue Game');
    }

    var audio = new Audio('https://objects.koji-cdn.com/fc1552bc-1ac2-4a30-bb6a-bdb100fe6835/deadsound.wav');
    if(soundFlag)
        audio.play();

    var spaceCanvas = document.getElementById('spaceCanvas');
    var ctx = spaceCanvas.getContext("2d");
    ctx.font = "bold 40px Comic Sans MS";
    ctx.fillStyle = "magenta";
    ctx.fillText("Caught You", 120, 150); 
    ctx.font = "bold 30px Comic Sans MS";
    ctx.fillStyle = "aqua";
    ctx.fillText("Lives left: "+life_left, 120, 250);
    ctx.fillText("Score: "+game_score,120, 350);
}

function gameManager(){
    var spaceCanvas = document.getElementById('spaceCanvas');
    spaceCanvas.height = 500;
    spaceCanvas.width = 500;
    var ctx = spaceCanvas.getContext("2d");
    ctx.font = "bold 50px Comic Sans MS";
    ctx.fillStyle = "magenta";
    ctx.fillText("Game Ready", 100, 250);

    if(typeof InstallTrigger !== 'undefined'){
        frame_speed = 5;
    }
    
    clickManager();
}

gameManager();

function clickManager(){
    document.getElementById('game_button').addEventListener("click", function(){
        startGame();
    });


    document.getElementById('up_button').addEventListener("pointerdown", function(){
        moveup();
    });

    document.getElementById('up_button').addEventListener("pointerup", function(){
        clearmove();
    });

    document.getElementById('up_button').addEventListener("ontouchstart", function(){
        moveup();
    });


    document.getElementById('sound_flag').addEventListener("click", function(){
        soundToggle();
    });


    document.getElementById('down_button').addEventListener("pointerdown", function(){
        movedown();
    });

    document.getElementById('down_button').addEventListener("pointerup", function(){
        clearmove();
    });

    document.getElementById('down_button').addEventListener("ontouchstart", function(){
        movedown();
    });

    window.addEventListener('keydown', function(event){
        if(event.keyCode === 38){
            event.preventDefault();
            moveup();
        }
        if(event.keyCode === 40){
            event.preventDefault();
            movedown();
        }
    });

    window.addEventListener('keyup', function(event){
        if(event.keyCode === 38 || event.keyCode === 40)
            clearmove();
    });    
}

function soundToggle(){
    var soundDisplay = document.getElementById('sound_flag');
    if(soundFlag){
        soundFlag = false;
        soundDisplay.innerHTML = 'Off &#128264;';
    }else{
        soundFlag = true;
        soundDisplay.innerHTML = 'On &#128266;'
    }
}

function fillCanvas(){
    hero_piece = new component(35, 35, "https://images.koji-cdn.com/fc1552bc-1ac2-4a30-bb6a-bdb100fe6835/peice.png", canvas_left+10, canvas_bottom/2, "image");
    game_space.start();
}

var game_space = {
    canvas : document.getElementById('spaceCanvas'),
    start : function() {
        this.context = this.canvas.getContext("2d");
        this.frameNo = 0;
        this.interval = setInterval(updateGameArea, frame_speed);
        },
    clear : function() {
        this.context.clearRect(0, 0, this.canvas.width, this.canvas.height);
    },
    stop : function() {
        clearInterval(this.interval);
    }
}

function component(width, height, color, x, y, type) {
    this.type = type;
    if (type == "image") {
        this.image = new Image();
        this.image.src = color;
    }
    this.width = width;
    this.height = height;
    this.speedX = 0;
    this.speedY = 0;    
    this.x = x;
    this.y = y;    
    this.update = function() {
        ctx = game_space.context;
        if (type == "image") {
            ctx.drawImage(this.image, this.x, this.y, this.width, this.height);
        } else{
            ctx.fillStyle = color;
            ctx.fillRect(this.x, this.y, this.width, this.height);
        }
    }
    this.newPos = function() {
        this.x += this.speedX;
        this.y += this.speedY;        
    }    
    this.crashWith = function(otherobj) {
        var piece_left = this.x;
        var piece_right= this.x + (this.width);
        var piece_top = this.y;
        var piece_bottom = this.y + (this.height);
        var otherleft = otherobj.x;
        var otherright = otherobj.x + (otherobj.width);
        var othertop = otherobj.y;
        var otherbottom = otherobj.y + (otherobj.height);
        var crash = true;
        if ((piece_bottom < othertop) || (piece_top > otherbottom) || (piece_right < otherleft) || (piece_left > otherright)) {
            crash = false;
        }
        return crash;
    }
    this.manageLevel = function(){
        if(game_level > 100){
            max_obstacle_per_frame = 8;
            min_obstacle_per_frame = 4;
            if(typeof InstallTrigger !== 'undefined'){
                frame_speed = 6;
            }else{
                frame_speed = 4;
            }
            fillCanvas();
        }
    }
}

function updateGameArea() {
    var random_popup_vertical, random_popup_horizon, random_obstacle, random_area, minArea, maxArea;
    var audio = new Audio('https://objects.koji-cdn.com/fc1552bc-1ac2-4a30-bb6a-bdb100fe6835/eatsound.wav');
    var score_board = document.getElementById('score_board');
    for (i = 0; i < obstacle_list.length; i += 1) {
        if (hero_piece.crashWith(obstacle_list[i])) {
            game_space.stop();
            reportCrash()
            return;
        }
        if(hero_piece.crashWith(eatable_list[i])){
            var eatable = eatable_list[i];
            if(eatable.width > 0){
                game_score += 1;
                if(soundFlag)
                    audio.play();
            }
            eatable.width = 0;
            eatable.height = 0;
            score_board.innerHTML = "Score: "+game_score;
        }
    }
    game_space.clear();
    game_space.frameNo += 1;
    if (game_space.frameNo == 1 || everyinterval(150)) {
        for(var i=0; i<Math.floor(Math.random()*(max_obstacle_per_frame - min_obstacle_per_frame)+min_obstacle_per_frame); i++){
            minArea = 35;
            maxArea = 65;
            random_obstacle = Math.floor(Math.random()*(4-1)+1);
            random_area = Math.floor(Math.random()*(maxArea-minArea+1)+minArea);
            random_popup_vertical = Math.floor(Math.random()*(canvas_bottom - canvas_top)+canvas_top);
            random_popup_horizon = Math.floor(Math.random()*(100-50 +1)+50);
            obstacle_list.push(new component(random_area, random_area, "https://images.koji-cdn.com/fc1552bc-1ac2-4a30-bb6a-bdb100fe6835/obstacle"+random_obstacle+".png", canvas_right+random_popup_horizon, random_popup_vertical,"image"));
            eatable_list.push(new component(25,25, "https://images.koji-cdn.com/fc1552bc-1ac2-4a30-bb6a-bdb100fe6835/eatable"+random_obstacle+".png", canvas_right+random_area+random_popup_horizon, random_popup_vertical+random_popup_horizon,"image"));
        }
    }
    for (i = 0; i < obstacle_list.length; i += 1) {
        obstacle_list[i].x += -1;
        eatable_list[i].x += -1;
        obstacle_list[i].update();
        eatable_list[i].update();
    }

    hero_piece.newPos();    
    hero_piece.update();
}

function everyinterval(n) {
    if ((game_space.frameNo / n) % 1 == 0) {return true;}
    return false;
}

function moveright() { 
    hero_piece.speedX = 1; 
}

function moveleft() {
    hero_piece.speedX = -1; 
}

function movedown() {
    if(hero_piece.y < 420){
        hero_piece.speedY = 1; 
    }
}

function moveup() {
    if(hero_piece.y > 30){
        hero_piece.speedY = -1;
    }
}

function clearmove() {
    hero_piece.speedX = 0; 
    hero_piece.speedY = 0; 
}