# Kresso

Pac space (Kresso) is a modified idea of PacMan where PacMan must dodge the ghosts to survive and eat the fruits to gain points.
The ghosts and fruits arrive randomly and PacMan must not go very close to the ghosts to avoid danger.
Currently the game is a single level game with limited assets and best experienced in FireFox and Chrome.

## Steps to run the game:
1. Open the folder having the game
2. Run the html file in your browser

## Steps to change assets:
1. Download or ready your assets of PNG format
2. Reame them to: Hero as peice obstacles as obstacles_1, obstacles_2, obstacles_3 and fruit to eatable
3. Put them in the assets folder of the game